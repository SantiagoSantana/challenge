import express, { Application, Request, Response } from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import fileUpload from 'express-fileupload';
import { createConnection } from 'typeorm';
import { AppRoutes } from '~/routes';

const PORT = process.env.PORT || 8080;

createConnection().then(async (connection) => {
  const app: Application = express();

  app.use(fileUpload({ createParentPath: true }));

  app.use(cors());
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));

  app.use(express.static(__dirname + '/public'));

  AppRoutes.forEach((route) => {
    app[route.method](
      route.path,
      (request: Request, response: Response, next: Function) => {
        route
          .action(request, response)
          .then(() => next)
          .catch((err: Error) => next(err));
      }
    );
  });

  app.listen(PORT, () => {
    console.log('Magic happens on port ' + PORT);
  });
});
