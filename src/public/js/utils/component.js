export class Component extends HTMLElement {
  /**
   * Initializes and assigns template to web component
   */
  constructor() {
    super();

    const template = document.getElementById(
      getTag(this.constructor.name) + '-template'
    );

    this.attachShadow({ mode: 'open' });
    this.shadowRoot.appendChild(template.content.cloneNode(true));
  }

  static get observedAttributes() {
    return [];
  }

  connectedCallback() {
    const attrs = this.constructor.observedAttributes;
    attrs.forEach((attr) => this._upgradeProperty(attr));
  }

  /**
   * Upgrades html attributes
   * https://developers.google.com/web/fundamentals/web-components/best-practices#lazy-properties
   */
  _upgradeProperty(prop) {
    if (this.hasOwnProperty(prop)) {
      let value = this[prop];
      delete this[prop];
      this[prop] = value;
    }
  }

  /**
   * sync properties when attributes change
   */
  attributeChangedCallback(attrName, oldValue, newValue) {
    // avoids infinite loop
    if (oldValue !== newValue) {
      this[attrName] = this.getAttribute(attrName);
    }
  }
}

/**
 * Converts class name to valid html tag
 * i.e 'MyComponent' to 'my-component'
 *
 * @param {string} component
 * @returns {string}
 */
function getTag(name) {
  const first = name[0].toLowerCase();
  const rest = name
    .slice(1, name.length)
    .replaceAll(/[A-Z]/g, (letter) => '-' + letter.toLowerCase());

  return first + rest;
}

/**
 * @param {typeof Component} component
 */
export function register(component) {
  customElements.define(getTag(component.name), component);
}
