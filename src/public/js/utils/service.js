// @ts-check
import './types.js';

const BASE_URL = 'http://localhost:6100';
export const ITEMS_URL = `${BASE_URL}/items`;

/**
 * @returns {Promise<Item[]>}
 */
export async function getItems() {
  const response = await fetch(ITEMS_URL);
  return await response.json();
}

/**
 * @param {string} description
 * @param {string} img
 *
 * @returns {Promise<Item>}
 */
export async function createItem(description, img) {
  const body = { description: description, img: img };

  const response = await fetch(ITEMS_URL, {
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(body),
  });

  return await response.json();
}

/**
 * @param {string} href
 * @param {File} file
 *
 * @returns {Promise<string>} id of image
 */
export async function uploadImg(href, file) {
  const data = new FormData();
  data.append('img', file);

  const response = await fetch(href, { method: 'post', body: data });

  return response.json();
}

/**
 * @param {string} id
 * @param {string} description
 * @param {string} img
 *
 * @returns {Promise}
 */
export async function editItem(id, description, img) {
  const body = { description: description, img: img };

  const response = await fetch(`${ITEMS_URL}/${id}`, {
    method: 'put',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(body),
  });

  return await response.text();
}

/**
 * @param {string} id
 *
 * @returns {Promise}
 */
export async function deleteItem(id) {
  const response = await fetch(`${ITEMS_URL}/${id}`, { method: 'delete' });
  return await response.text();
}

/**
 * @param {Array} moves
 *
 * @returns {Promise}
 */
export async function reorder(moves) {
  const body = { moves: moves };

  const response = await fetch(`${ITEMS_URL}/reorder`, {
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(body),
  });

  return response;
}
