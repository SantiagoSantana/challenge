// @ts-check

import { ListItem } from '../../components/list-item/index.js';
import { reorder } from './service.js';

export class Sortable {
  /**
   * @param {HTMLElement} list
   */
  constructor(list) {
    this.list = list;
    this.dragSource = null;
    this.lastTarget = null;
    this.moves = [];
    this.sourceBoundingRect = null;
    this._events = this._makeEvents();

    const items = /** @type {NodeListOf<ListItem>} */ (list.childNodes);
    this._makeItemsDraggable(items);

    this.list.addEventListener('dragstart', this._events.start);
  }

  _makeItemsDraggable(items) {
    items.forEach((item) => {
      item.draggable = true;
      item.$itemImg && (item.$itemImg.draggable = false);
    });

    const itemsAddedObserver = new MutationObserver((mutations) => {
      mutations.forEach((mutation) => {
        mutation.addedNodes.forEach((node) => {
          if (node instanceof ListItem) {
            node.draggable = true;
            node.$itemImg.draggable = false;
          }
        });
      });
    });

    itemsAddedObserver.observe(this.list, { childList: true });
  }

  _makeEvents() {
    return {
      /** @param {DragEvent} event */
      start: (event) => {
        const target = /** @type {ListItem} */ (event.target);
        target.style.opacity = '0.5';

        this.dragSource = target;
        this.lastTarget = this.dragSource;
        this.sourceBoundingRect = target.getBoundingClientRect();

        event.dataTransfer.effectAllowed = 'move';

        this.list.addEventListener('dragover', this._events.over);
        this.list.addEventListener('dragend', this._events.end);
        this.list.addEventListener('drop', this._events.drop);
      },

      /** @param {DragEvent} event */
      over: (event) => {
        event.preventDefault();

        event.dataTransfer.dropEffect = 'move';

        const target = /** @type {ListItem} */ (event.target);

        if (
          target &&
          target instanceof ListItem &&
          this.dragSource.id !== target.id &&
          this.lastTarget.id !== target.id
        ) {
          this.lastTarget = target;

          const moveTo = () => {
            return event.clientY < this.sourceBoundingRect.top
              ? target
              : target.nextSibling;
          };

          // swap values
          [this.dragSource.order, target.order] = [
            target.order,
            this.dragSource.order,
          ];

          this.moves.push({ id: target.id, order: target.order });

          this.list.insertBefore(this.dragSource, moveTo());
        }
      },

      /** @param {DragEvent} event */
      drop: (event) => {
        this.moves.push({
          id: this.dragSource.id,
          order: this.dragSource.order,
        });
        reorder(this.moves).catch(() => {});
      },

      /** @param {DragEvent} event */
      end: (event) => {
        event.preventDefault();

        const target = /** @type {HTMLElement} */ (event.target);
        target.style.opacity = '1';

        this.list.removeEventListener('dragover', this._events.over);
        this.list.removeEventListener('dragend', this._events.end);
        this.list.removeEventListener('drop', this._events.drop);
      },
    };
  }
}
