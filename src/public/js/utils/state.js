// @ts-check

/**
 * @readonly
 * @enum {'initial'|'uploading'|'uploaded'|'error'}
 */
const UPLOAD_IMAGE_STATES = {
  initial: /** @type {'initial'} */ ('initial'),
  uploading: /** @type {'uploading'} */ ('uploading'),
  uploaded: /** @type {'uploaded'} */ ('uploaded'),
  error: /** @type {'uploaded'} */ ('error'),
};

/**
 * @readonly
 * @enum {'empty'|'nonempty'}
 */
const DESCRIPTION_STATES = {
  empty: /** @type {'empty'} */ ('empty'),
  nonempty: /** @type {'nonempty'} */ ('nonempty'),
};

/**
 * @readonly
 * @enum {'initial'|'loading'|'loaded'|'error'}
 */
const ITEMS_STATES = {
  initial: /** @type {'initial'} */ ('initial'),
  loading: /** @type {'loading'} */ ('loading'),
  loaded: /** @type {'loaded'} */ ('loaded'),
  error: /** @type {'error'} */ ('error'),
};

export class State {
  /**
   * @param {StateListener[]} listeners
   */
  constructor(listeners) {
    /** @type {UPLOAD_IMAGE_STATES} */
    this._imageValue = UPLOAD_IMAGE_STATES.initial;

    /** @type {DESCRIPTION_STATES} */
    this._descriptionValue = DESCRIPTION_STATES.empty;

    /** @type {ITEMS_STATES} */
    this._itemsValue = ITEMS_STATES.initial;

    this.listeners = listeners;
  }

  /**
   * @param {UPLOAD_IMAGE_STATES} value
   * @returns {void}
   */
  set _image(value) {
    this._imageValue = value;
    this.listeners.forEach((listener) => listener(this, 'image'));
  }

  /**
   * @param {DESCRIPTION_STATES} value
   * @returns {void}
   */
  set _description(value) {
    this._descriptionValue = value;
    this.listeners.forEach((listener) => listener(this, 'description'));
  }

  /**
   * @param {ITEMS_STATES} value
   * @returns {void}
   */
  set _items(value) {
    this._itemsValue = value;
    this.listeners.forEach((listener) => listener(this, 'items'));
  }

  imageReset() {
    this._image = UPLOAD_IMAGE_STATES.initial;
  }

  imageUploading() {
    this._image = UPLOAD_IMAGE_STATES.uploading;
  }

  imageUploaded() {
    this._image = UPLOAD_IMAGE_STATES.uploaded;
  }

  imageUploadError() {
    this._image = UPLOAD_IMAGE_STATES.error;
  }

  get isImageInitial() {
    return this._imageValue === UPLOAD_IMAGE_STATES.initial;
  }

  get isImageUploading() {
    return this._imageValue === UPLOAD_IMAGE_STATES.uploading;
  }

  get isImageUploaded() {
    return this._imageValue === UPLOAD_IMAGE_STATES.uploaded;
  }

  descriptionReset() {
    this._description = DESCRIPTION_STATES.empty;
  }

  descriptionSetted() {
    this._description = DESCRIPTION_STATES.nonempty;
  }

  get hasDescription() {
    return this._descriptionValue === DESCRIPTION_STATES.nonempty;
  }

  get formCompleted() {
    return this.isImageUploaded && this.hasDescription;
  }

  itemsLoading() {
    this._items = ITEMS_STATES.loading;
  }

  itemsLoaded() {
    this._items = ITEMS_STATES.loaded;
  }

  itemsError() {
    this._items = ITEMS_STATES.error;
  }

  itemsReset() {
    this._items = ITEMS_STATES.initial;
  }

  get isItemsLoaded() {
    return this._itemsValue === ITEMS_STATES.loaded;
  }

  get isItemsError() {
    return this._itemsValue === ITEMS_STATES.error;
  }

  reset() {
    this.imageReset();
    this.descriptionReset();
    this.itemsReset();
  }
}
