// @ts-check

/**
 * @typedef {{ id: string; description: string; order: string }} Item
 */

/**
 * @callback StateListener
 * @param {import('./state').State} state
 * @param {'items'|'description'|'image'} updateAttr
 * @returns {void}
 */
