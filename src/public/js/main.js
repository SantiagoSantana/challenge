// @ts-check

import {
  CounterItems,
  CustomSpinner,
  EditItemDialog,
  ImageUpload,
  createItem,
  ListItem,
  DescriptionInput,
} from '../components/index.js';
import { Sortable } from './utils/sort.js';
import { Component, register } from './utils/component.js';
import { createItem as createItemService, getItems } from './utils/service.js';
import { State } from './utils/state.js';
// @ts-ignore
import { GenericAlert } from 'https://unpkg.com/@thepassle/generic-components@0.2.1/index.js';

/** @type {typeof Component[]} */
const webComponents = [
  ListItem,
  CounterItems,
  ImageUpload,
  CustomSpinner,
  EditItemDialog,
  GenericAlert,
  DescriptionInput,
];

webComponents.forEach((webComponent) => register(webComponent));

// @ts-ignore
ally.style.focusWithin();

/** @param {string} id */
const getElement = (id) => document.getElementById(id);

const elements = {
  $img: /** @type {ImageUpload} */ (getElement('add-item-img')),
  $text: /** @type {DescriptionInput} */ (getElement('add-item-text')),
  $button: /** @type {HTMLButtonElement} */ (getElement('add-item-button')),
  $counter: /** @type {CounterItems} */ (getElement('counter')),
  list: /** @type {HTMLLIElement} */ (getElement('list')),
  form: /** @type {HTMLFormElement} */ (getElement('add-item')),
};

/** @type {StateListener} */
function updateAddItemButtonState(state) {
  elements.$button.disabled = !state.formCompleted;
}

/** @type {StateListener} */
function handleItemsState(state, updatedAttr) {
  if (updatedAttr !== 'items') {
    return;
  }

  if (state.isItemsLoaded || state.isItemsError) {
    const spinner = elements.list.querySelector('#spinner');
    if (spinner) {
      spinner.remove();
    }
  }

  if (state.isItemsError) {
    const alert = document.createElement('generic-alert');
    alert.textContent = 'Please reload the page';
    elements.list.prepend(alert);
  }
}

const state = new State([updateAddItemButtonState, handleItemsState]);

elements.$img.addEventListener('begin-image-upload', () => {
  state.imageUploading();
});

elements.$img.addEventListener('end-image-upload', () => {
  state.imageUploaded();
});

elements.$img.addEventListener('error-image-upload', () => {
  state.imageUploadError();
});

elements.$text.addEventListener('input', (event) => {
  const target = /** @type {DescriptionInput} */ (event.target);

  if (target.description.length === 0) {
    state.descriptionReset();
  } else {
    state.descriptionSetted();
  }
});

elements.form.addEventListener('submit', async (event) => {
  event.preventDefault();

  const item = await createItemService(
    elements.$text.description,
    elements.$img.imgId
  );

  appendItem(item);

  elements.$img.img = '';
  elements.$text.description = '';

  state.reset();
});

elements.list.addEventListener('item-deleted', removeItem);

/**
 * @param {Item} item
 */
function appendItem(item) {
  const node = createItem(item.id, item.description, item.order);
  node.style.opacity = '0';
  elements.list.prepend(node);
  node.animate(
    { opacity: [0, 1] },
    { duration: 400, easing: 'ease-out', fill: 'forwards' }
  );
  elements.$counter.add(1);
}

/**
 * @param {CustomEvent} event
 */
async function removeItem(event) {
  const item = event.detail.item;
  await item.animate(
    { opacity: [1, 0] },
    { duration: 400, easing: 'ease-out', fill: 'forwards' }
  ).finished;

  elements.list.removeChild(item);
  elements.$counter.add(-1);
}

const spinner = document.createElement('custom-spinner');
spinner.id = 'spinner';

elements.list.append(spinner);

state.itemsLoading();
getItems()
  .then(async (items) => {
    items.forEach((item) => appendItem(item));
    new Sortable(elements.list);
    state.itemsLoaded();
  })
  .catch(() => {
    state.itemsError();
  });
