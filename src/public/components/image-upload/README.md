# ImageUpload Element

## Attributes

- img
- href

## Properties

- img
- href
- imgId (readonly)
- file (readonly)
