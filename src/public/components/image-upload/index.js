// @ts-check
import { Component } from '../../js/utils/component.js';
import { uploadImg } from '../../js/utils/service.js';

/**
 * @readonly
 * @enum {'begin-image-upload'|'end-image-upload'|'error-image-upload'}
 */
const IMAGE_UPLOAD_EVENTS = {
  BEGIN: /** @type {'begin-image-upload'} */ ('begin-image-upload'),
  END: /** @type {'end-image-upload'} */ ('end-image-upload'),
  ERROR: /** @type {'error-image-upload'} */ ('error-image-upload'),
};

export class ImageUpload extends Component {
  constructor() {
    super();

    /** @type {string} */
    this._imgId = null;

    this.events = {
      /** @param {Event} event */
      over: (event) => {
        event.stopPropagation();
        event.preventDefault();
      },
      /** @param {Event} event */
      leave: (event) => {
        event.preventDefault();
      },
      click: () => {
        this.$input.click();
      },
      change: () => {
        this.updateImg();
      },
      /** @param {DragEvent} event */
      drop: (event) => {
        event.preventDefault();

        const files = event.dataTransfer.files;
        const hasFiles = files.length > 0;

        if (!hasFiles) {
          return;
        }

        this.$input.files = files;
        this.updateImg();
      },
    };
  }

  static get observedAttributes() {
    return ['img', 'href'];
  }

  connectedCallback() {
    /**
     * Avoid FOUC in chrome browser
     */
    const style = document.createElement('style');
    style.innerHTML = ':host { display: none; }';
    this.shadowRoot.prepend(style);

    this.addEventListener('dragover', this.events.over);
    this.addEventListener('dragleave', this.events.leave);
    this.addEventListener('dragend', this.events.leave);
    this.addEventListener('drop', this.events.drop);

    /** @type {HTMLInputElement}  */
    this.$input = this.shadowRoot.querySelector('.file-input');
    /** @type {HTMLElement}  */
    this.$label = this.shadowRoot.querySelector('.file-label');
    this.$imgElement = this.shadowRoot.querySelector('.file-img');

    this.$input.addEventListener('change', this.events.change);

    super.connectedCallback();
  }

  disconnectedCallback() {
    this.removeEventListener('dragover', this.events.over);
    this.removeEventListener('dragleave', this.events.leave);
    this.removeEventListener('dragend', this.events.leave);
    this.removeEventListener('drop', this.events.drop);
    this.removeEventListener('click', this.events.click);
  }

  get img() {
    return this.getAttribute('img');
  }

  set img(value) {
    this.setAttribute('img', value);

    if (value) {
      this.$label.classList.add('hidden');
      this.addEventListener('click', this.events.click);
      this.$imgElement.setAttribute('src', value);
      this.$imgElement.classList.remove('hidden');
    } else {
      this.clear();
    }
  }

  get file() {
    return this.$input.files[0];
  }

  set href(value) {
    this.setAttribute('href', value);
  }

  get href() {
    return this.getAttribute('href');
  }

  get imgId() {
    return this._imgId;
  }

  clear() {
    this._imgId = null;
    this.removeEventListener('click', this.events.click);
    this.$label.classList.remove('hidden');
    this.$imgElement.classList.add('hidden');
    this.$label.innerText = 'Click here or drag a file.';
  }

  async updateImg() {
    this.img = '';
    this._imgId = null;
    this.$label.innerHTML = '<custom-spinner></custom-spinner>';

    const dispatch = (eventName) =>
      this.dispatchEvent(new CustomEvent(eventName, { bubbles: true }));

    try {
      dispatch(IMAGE_UPLOAD_EVENTS.BEGIN);
      this._imgId = await this.uploadFile();
      dispatch(IMAGE_UPLOAD_EVENTS.END);

      this.img = URL.createObjectURL(this.file);
    } catch (error) {
      dispatch(IMAGE_UPLOAD_EVENTS.ERROR);
    }
  }

  /**
   * @returns {Promise<string>}
   */
  async uploadFile() {
    let id;

    try {
      id = await uploadImg(this.href, this.file);
    } catch (error) {
      throw error;
    }

    return id;
  }
}
