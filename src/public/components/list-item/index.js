// @ts-check

import { Component } from '../../js/utils/component.js';
// @ts-ignore
import { dialog } from 'https://unpkg.com/@thepassle/generic-components@0.2.1/index.js';
import { EditItemDialog } from '../edit-item-dialog/index.js';
import {
  ITEMS_URL,
  deleteItem as deleteItemService,
} from '../../js/utils/service.js';

export class ListItem extends Component {
  constructor() {
    super();

    /** @type {HTMLImageElement} */
    this.$itemImg = this.shadowRoot.querySelector('.item-img');

    /** @type {HTMLButtonElement} */
    this.$editBtn = this.shadowRoot.querySelector('#btn-update');

    /** @type {HTMLButtonElement} */
    this.$deleteBtn = this.shadowRoot.querySelector('#btn-delete');

    this.deleteItem = (event) => this._deleteItem(event);
    this.openDialog = (event) => this._openDialog(event);
  }

  static get observedAttributes() {
    return ['img', 'description', 'order'];
  }

  connectedCallback() {
    /**
     * Avoid FOUC in chrome browser
     */
    const style = document.createElement('style');
    style.innerHTML = `
      .item-img {
        display: block;
      }

      .list-item {
        cursor: grab;
        border-radius: 10px;
        box-shadow: rgba(0, 0, 0, 0.15) 0px 2px 6px;
        display: flex;
        flex-direction: column;
        align-items: center;
        margin: 0.5rem 0;
        padding: 16px;
        transition: box-shadow 0.2s ease 0s;
        min-width: var(--container-size);
      }

      .list-item:hover {
        box-shadow: rgba(0, 0, 0, 0.1) 0px 6px 12px;
      }

      .item-img {
        height: var(--img-size);
        width: var(--img-size);
        flex-shrink: 0;
        object-fit: cover;
        border-radius: 0.25em;
      }

      .item-description {
        text-align: left;
        margin: 0 1rem;
      }

      .list-item-content {
        display: flex;
        flex-direction: column;
        align-items: center;
        width: 100%;
        justify-content: space-between;
      }

      .btn {
        border: none;
        border-radius: 100%;
        padding: 1em 1em;
        background-color: #fff;
      }

      @media screen and (min-width: 700px) {
        .list-item {
          flex-direction: row;
          justify-content: flex-start;
          align-items: flex-start;
        }
        .list-item-content {
          flex-direction: row;
        }
      }
    `;

    this.shadowRoot.prepend(style);

    this.role = 'listitem';

    this.$editBtn.addEventListener('click', this.openDialog);
    this.$deleteBtn.addEventListener('click', this.deleteItem);

    this.$itemImg.addEventListener(
      'error',
      () => (this.img = 'https://via.placeholder.com/160')
    );

    super.connectedCallback();
  }

  disconnectedCallback() {
    this.$editBtn.removeEventListener('click', this.openDialog);
  }

  async _deleteItem(event) {
    try {
      await deleteItemService(this.id);
      this.dispatchEvent(
        new CustomEvent('item-deleted', {
          bubbles: true,
          detail: { item: this },
        })
      );
    } catch (error) {
      throw error;
    }
  }

  _openDialog(event) {
    const handleUploadItem = (event) => {
      this.img = `${this.$itemImg.src}?${new Date().getTime()}`;
      this.description = event.detail.description;
      dialog.close();
    };

    dialog.open({
      invokerNode: event.target,
      content: (dialogNode) => {
        const edit = /** @type {EditItemDialog} */ (document.createElement(
          'edit-item-dialog'
        ));

        edit.itemId = this.id;
        edit.description = this.description;
        edit.img = this.img;

        const container = document.createElement('div');
        container.appendChild(edit);

        dialogNode.appendChild(container);

        dialogNode.addEventListener('item-updated', handleUploadItem);
      },
    });
  }

  get img() {
    return this.getAttribute('img');
  }

  set img(value) {
    this.setAttribute('img', value);

    if (this.$itemImg) {
      this.$itemImg.src = value;
    }
  }

  get description() {
    return this.innerText;
  }

  set description(value) {
    this.setAttribute('description', value);
    this.innerText = value;
  }

  get order() {
    return this.getAttribute('order');
  }

  set order(value) {
    this.setAttribute('order', value);
  }

  delete() {
    if (!this.id) {
      throw Error('An item must have an id to be deleted');
    }
  }
}

/**
 * @param {string} id
 * @param {string} description
 * @param {string} order
 */
export function createItem(id, description, order) {
  const img = `${ITEMS_URL}/${id}/media`;

  const node = /** @type {ListItem} */ (document.createElement('list-item'));

  node.id = id;
  node.description = description;
  node.img = img;
  node.order = order;

  return node;
}
