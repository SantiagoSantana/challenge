// @ts-check

import { Component } from '../../js/utils/component.js';
import { editItem } from '../../js/utils/service.js';
import { DescriptionInput } from '../description-input/index.js';
import { ImageUpload } from '../image-upload/index.js';

class EditItemDialogState {
  /**
   * @param {EditItemDialog} editItemDialog
   */
  constructor(editItemDialog) {
    this.dialog = editItemDialog;

    /* @param {boolean} value */
    const createProxy = (value) =>
      new Proxy(
        { state: value },
        {
          set: (obj, prop, value) => {
            obj[prop] = value;
            if (prop === 'state') {
              this.updateButton();
            }
            return true;
          },
        }
      );

    this.imageOk = createProxy(true);
    this.valueChanged = createProxy(false);

    this.listeners = {
      beginUpload: () => {
        this.imageOk.state = false;
      },
      endUpload: () => {
        this.imageOk.state = true;
        this.valueChanged.state = true;
      },
      errorUpload: () => {
        this.imageOk.state = false;
      },
      input: () => {
        this.valueChanged.state = true;
      },
      submit: async (/** @type {Event} */ event) => {
        event.preventDefault();

        await editItem(
          this.dialog.itemId,
          this.dialog.description,
          this.dialog.$img.imgId
        );

        const updatedItem = new CustomEvent('item-updated', {
          bubbles: true,
          detail: {
            id: this.dialog.itemId,
            description: this.dialog.description,
          },
        });

        this.imageOk.state = false;
        this.valueChanged.state = false;

        this.dialog.dispatchEvent(updatedItem);
      },
    };
  }

  updateButton() {
    if (!this.imageOk.state) {
      this.dialog.$button.disabled = true;
    }

    this.dialog.$button.disabled = !this.valueChanged.state;

    console.log(this.dialog.$button);
    console.log(this.dialog.$button.disabled);
  }

  connect() {
    this.dialog.$img.addEventListener(
      'begin-image-upload',
      this.listeners.beginUpload
    );
    this.dialog.$img.addEventListener(
      'end-image-upload',
      this.listeners.endUpload
    );
    this.dialog.$img.addEventListener(
      'error-image-upload',
      this.listeners.errorUpload
    );
    this.dialog.$form.addEventListener('submit', this.listeners.submit);
    this.dialog.$description.addEventListener('input', this.listeners.input);
  }

  disconnect() {
    this.dialog.$img.removeEventListener(
      'begin-image-upload',
      this.listeners.beginUpload
    );
    this.dialog.$img.removeEventListener(
      'end-image-upload',
      this.listeners.endUpload
    );
    this.dialog.$img.removeEventListener(
      'error-image-upload',
      this.listeners.errorUpload
    );
    this.dialog.$form.removeEventListener('submit', this.listeners.submit);
    this.dialog.removeEventListener('input', this.listeners.input);
  }
}

export class EditItemDialog extends Component {
  constructor() {
    super();

    this.state = new EditItemDialogState(this);

    /** @type {DescriptionInput} */
    this.$description = this.shadowRoot.querySelector('#add-item-text');

    /** @type {ImageUpload} */
    this.$img = this.shadowRoot.querySelector('#add-item-img');

    /** @type {HTMLFormElement} */
    this.$form = this.shadowRoot.querySelector('#add-item');

    /** @type {HTMLButtonElement} */
    this.$button = this.shadowRoot.querySelector('#update-button');
    this.$button.disabled = true;
  }

  static get observedAttributes() {
    return ['item-id', 'img', 'description'];
  }

  connectedCallback() {
    this.state.connect();
    super.connectedCallback();
  }

  disconnectedCallback() {
    this.state.disconnect();
  }

  get itemId() {
    return this.getAttribute('item-id');
  }

  set itemId(value) {
    this.setAttribute('item-id', value);
  }

  get description() {
    return this.$description.description;
  }

  set description(value) {
    this.$description.setAttribute('description', value);
    this.$description.description = value;
  }

  get img() {
    return this.getAttribute('img');
  }

  set img(value) {
    this.$img.img = value;
    this.setAttribute('img', value);
  }
}
