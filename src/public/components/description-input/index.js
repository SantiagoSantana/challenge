// @ts-check

import { Component } from '../../js/utils/component.js';

const MAX_SIZE = 300;

export class DescriptionInput extends Component {
  constructor() {
    super();

    /** @type {HTMLTextAreaElement} */
    this.$description = this.shadowRoot.querySelector('#add-item-text');
    this.$description.maxLength = MAX_SIZE;

    this.description = '';
  }

  static get observedAttributes() {
    return ['description'];
  }

  connectedCallback() {
    super.connectedCallback();
  }

  get chars() {
    return MAX_SIZE - this.description.length;
  }

  get description() {
    return this.$description.value;
  }

  set description(value) {
    if (value.length > MAX_SIZE) {
      return;
    }

    this.$description.value = value;

    this.setAttribute('description', value);
  }
}
