// @ts-check
import { Component } from '../../js/utils/component.js';

export class CounterItems extends Component {
  constructor() {
    super();
  }

  static get observedAttributes() {
    return ['value'];
  }

  connectedCallback() {
    /** @type {HTMLElement} */
    this.$subtitle = this.shadowRoot.querySelector('.subtitle');
    this.value = (0).toString();
    super.connectedCallback();
  }

  get value() {
    return this.getAttribute('value');
  }

  set value(value) {
    this.setAttribute('value', value);
    this.$subtitle.innerText = `${value} items`;
  }

  /**
   * @param {number} value
   */
  add(value) {
    this.value = (Number.parseInt(this.value) + value).toString();
  }
}
