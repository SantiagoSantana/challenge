import { Request, Response } from 'express';
import { getManager } from 'typeorm';
import { Item } from '~/entity/Item';

/**
 * Edit an item by id
 */
export async function itemDelete(request: Request, response: Response) {
  const manager = getManager();

  const itemRepo = manager.getMongoRepository(Item);
  const id = request.params.id;

  if (!id) {
    response.status(404).send();
    return;
  }

  const item = await itemRepo.findOne(id);

  if (!item) {
    response.status(400).send({ message: `Item with id ${id} does not exist` });
    return;
  }

  await itemRepo.delete(item);
  response.send('item deleted successfully');
}
