import { Request, Response } from 'express';
import { getManager } from 'typeorm';
import { Item } from '~/entity/Item';
import { Media } from '~/entity/Media';

/**
 * Adds a new item to the database
 */
export async function itemAdd(request: Request, response: Response) {
  const manager = getManager();
  const itemRepo = manager.getMongoRepository(Item);
  const mediaRepo = manager.getMongoRepository(Media);

  const { description, img } = request.body;

  const item = new Item();

  item.description = description;
  item.order = (await itemRepo.count()) + 1;

  /**
   * `findOne` will return a result if condition is `undefined` or `null`
   * https://github.com/typeorm/typeorm/issues/2500
   */
  if (img) {
    const media = await mediaRepo.findOne(img);
    if (media) {
      item.img = {
        media_type: media.media_type ?? '',
        filename: media.filename ?? '',
      };
    }
  }

  await itemRepo.save(item);

  response.send(item);
}
