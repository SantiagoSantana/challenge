import { Request, Response } from 'express';
import { ObjectID } from 'mongodb';
import { getManager } from 'typeorm';
import { Item } from '~/entity/Item';

/**
 * Edit an item by id
 */
export async function itemReorder(request: Request, response: Response) {
  const manager = getManager();

  const itemRepo = manager.getMongoRepository(Item);

  const { moves } = request.body;

  type Move = { id: string; order: string };

  moves.forEach((move: Move) => {
    console.info(move);
    itemRepo.findOneAndUpdate(
      { _id: ObjectID.createFromHexString(move.id) },
      { $set: { order: Number.parseInt(move.order) } }
    );
  });

  response.status(200).send();
}
