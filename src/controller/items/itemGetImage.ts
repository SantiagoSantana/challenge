import { Request, Response } from 'express';
import { getManager } from 'typeorm';
import { Item } from '~/entity/Item';

export async function itemGetImage(request: Request, response: Response) {
  const itemRepository = getManager().getMongoRepository(Item);

  const item = await itemRepository.findOne(request.params.id);

  if (!item || !item.img) {
    response.status(404).send({
      message: 'Item not found',
    });

    return;
  }

  const options = {
    root: `/home/node/imgs`,
    headers: { 'Content-Type': item.img.media_type },
  };

  response.sendFile(item.img.filename, options);
}
