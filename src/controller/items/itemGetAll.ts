import { Request, Response } from 'express';
import { getManager } from 'typeorm';
import { Item } from '~/entity/Item';

/**
 * Loads all items from the database.
 */
export async function itemGetAll(_: Request, response: Response) {
  const itemRepository = getManager().getMongoRepository(Item);

  const items = await itemRepository.find({ order: { order: 'ASC' } });

  const selectAttrs = (item: Item) => ({
    id: item.id,
    description: item.description,
    order: item.order,
  });

  response.send(items.map((item) => selectAttrs(item)));
}
