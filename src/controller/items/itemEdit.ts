import { Request, Response } from 'express';
import { getManager } from 'typeorm';
import { Item } from '~/entity/Item';
import { Media } from '~/entity/Media';

/**
 * Edit an item by id
 */
export async function itemEdit(request: Request, response: Response) {
  const manager = getManager();

  const itemRepo = manager.getMongoRepository(Item);
  const mediaRepo = manager.getMongoRepository(Media);
  manager.findOne(Item);

  const id = request.params.id;
  const { description, img } = request.body;

  if (!id) {
    response.status(404).send();
    return;
  }

  const item = await itemRepo.findOne(id);

  if (!item) {
    response.status(400).send({ message: `Item with id ${id} does not exist` });
    return;
  }

  item.description = description;

  if (img) {
    const media = await mediaRepo.findOne(img);
    if (media) {
      item.img = {
        media_type: media.media_type ?? '',
        filename: media.filename ?? '',
      };
    }
  }

  await itemRepo.save(item);

  response.send('item updated successfully');
}
