import cryptoRandomString from 'crypto-random-string';
import { Request, Response } from 'express';
import { UploadedFile } from 'express-fileupload';
import { getManager } from 'typeorm';
import { Media } from '~/entity/Media';

/**
 * Adds a new item to the database
 */
export async function mediaPost(request: Request, response: Response) {
  if (!request.files || !request.files.img) {
    response.status(400).send({ message: 'No file uploaded' });
    return;
  }

  const image = request.files.img as UploadedFile;

  const filename = cryptoRandomString({ length: 15, type: 'alphanumeric' });
  try {
    await image.mv(`/home/node/imgs/${filename}`);
  } catch (error) {
    response.status(500).send({ message: 'Error when saving image' });
  }

  const repository = getManager().getMongoRepository(Media);

  const media = new Media();
  media.media_type = image.mimetype;
  media.filename = filename;

  try {
    await repository.save(media);
  } catch (error) {
    response.status(500).send({ message: 'Error when saving image' });
  }

  response.send(media.id);
}
