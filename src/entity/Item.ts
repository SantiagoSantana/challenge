import { Entity, Column, ObjectIdColumn } from 'typeorm';

class ItemMedia {
  @Column()
  media_type: string;

  @Column()
  filename: string;

  constructor(media_type: string, filename: string) {
    this.media_type = media_type;
    this.filename = filename;
  }
}

@Entity()
export class Item {
  @ObjectIdColumn()
  id?: number;

  @Column()
  description?: string;

  @Column()
  order?: number;

  @Column(() => ItemMedia)
  img?: ItemMedia;
}
