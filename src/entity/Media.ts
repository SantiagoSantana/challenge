import { Entity, Column, ObjectIdColumn } from 'typeorm';

@Entity()
export class Media {
  @ObjectIdColumn()
  id?: number;

  @Column()
  media_type?: string;

  @Column()
  filename?: string;
}
