import { itemGetAll } from '~/controller/items/itemGetAll';
import { itemAdd } from '~/controller/items/itemAdd';
import { itemEdit } from '~/controller/items/itemEdit';
import { itemReorder } from '~/controller/items/itemReorder';
import { itemGetImage } from '~/controller/items/itemGetImage';
import { itemDelete } from '~/controller/items/itemDelete';
import { mediaPost } from '~/controller/media/mediaPost';

interface AppRoute {
  path: string;
  method:
    | 'all'
    | 'get'
    | 'post'
    | 'put'
    | 'delete'
    | 'patch'
    | 'options'
    | 'head';
  action: Function;
}

/**
 * All application routes.
 */
export const AppRoutes: AppRoute[] = [
  {
    path: '/items',
    method: 'get',
    action: itemGetAll,
  },
  {
    path: '/items',
    method: 'post',
    action: itemAdd,
  },
  {
    path: '/items/:id',
    method: 'put',
    action: itemEdit,
  },
  {
    path: '/items/:id',
    method: 'delete',
    action: itemDelete,
  },
  {
    path: '/items/:id/media',
    method: 'get',
    action: itemGetImage,
  },
  {
    path: '/items/reorder',
    method: 'post',
    action: itemReorder,
  },
  {
    path: '/media',
    method: 'post',
    action: mediaPost,
  },
];
