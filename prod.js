const tsconfig = require('./tsconfig.json');
const tsconfigPaths = require('tsconfig-paths');

tsconfigPaths.register({
  baseUrl: './dist',
  paths: tsconfig.compilerOptions.paths,
});

require('./server.js');
