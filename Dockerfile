# PRODUCTION DOCKERFILE
# ---------------------
# Sourced from: https://github.com/Saluki/nestjs-template/blob/master/Dockerfile
# 
# This Dockerfile allows to build a Docker image of the Express application
# and based on a NodeJS 12 image. The multi-stage mechanism allows to build
# the application in a "builder" stage and then create a lightweight production
# image containing the required dependencies and the JS build files.
# 
# Dockerfile best practices
# https://docs.docker.com/develop/develop-images/dockerfile_best-practices/
# Dockerized NodeJS best practices
# https://github.com/nodejs/docker-node/blob/master/docs/BestPractices.md
# https://www.bretfisher.com/node-docker-good-defaults/
# http://goldbergyoni.com/checklist-best-practice-of-node-js-in-production/

FROM node:12-alpine as builder

ENV NODE_ENV build

USER node
WORKDIR /home/node

COPY . /home/node

RUN npm ci \
    && npm run build \
    && cp /home/node/prod.js /home/node/tsconfig.json /home/node/dist/ \
    && cp -r /home/node/src/public /home/node/dist/public
 
# ---

FROM node:12-alpine

ENV NODE_ENV production

# https://github.com/moby/moby/issues/2259#issuecomment-48286811
RUN mkdir -p /home/node/imgs ; chown 1000:1000 /home/node/imgs

USER node
WORKDIR /home/node

COPY --from=builder /home/node/package*.json /home/node/
COPY --from=builder /home/node/dist/ /home/node/dist/

RUN npm ci

CMD ["node", "dist/prod.js"]
